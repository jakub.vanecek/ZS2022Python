
firstInputNumber = int(input('Insert first number:'))
secondInputNumber = int(input('Insert second number:'))


def compare_numbers(first_number, second_number):
    if first_number >= second_number:
        greater_number = first_number
        lower_number = second_number
    else:
        greater_number = second_number
        lower_number = first_number

    return greater_number, lower_number


def get_gcd(first_number, second_number):
    greater_number, lower_number = compare_numbers(first_number, second_number)

    if greater_number == 0 or lower_number == 0:
        return
    elif greater_number % lower_number == 0:
        return lower_number
    else:
        while lower_number != 0:
            if greater_number % lower_number == 0:
                return lower_number
            else:
                helper = greater_number
                greater_number = lower_number
                lower_number = helper - (helper // lower_number) * lower_number
        return greater_number


result = get_gcd(firstInputNumber, secondInputNumber)

if result is not None:
    print("Nejmensi spolecny delitel cisel " + str(firstInputNumber) + ' a ' + str(secondInputNumber) + ' je :' + str(result))
else:
    print("Jedno ze zadanych cisel je 0, spolecneho delitele tedy nelze stanovit")

