numberInput = input('Zadej cislo, ktere chces rozlozit na prvocisla: ')

primes = []


def check_input_validity(value_to_check):
    try:
        value_to_check = int(value_to_check)
    except:
        print('Vstupni parametr neni cele cislo')
        exit()
    if value_to_check == 0 or value_to_check == 1:
        print('Vstupni parametr je ' + str(value_to_check) + '. Rozdelini na prvocisla nelze provest')
        exit()
    elif check_if_prime(value_to_check) is True:
        print('Vstupni parametr je prvocislo')
        exit()
    return(value_to_check)


def check_if_prime(value_to_check):
    if value_to_check == 2:
        return True
    for x in range(2 , value_to_check):
        if value_to_check % x == 0:
            return False
    return True


def number_into_primes(number):
    while check_if_prime(number) is not True:
        for x in range(2,number):
            if number % x == 0:
                primes.append(int(x))
                number = number // x
                break
    primes.append(number)
    print(primes)
    exit()


def run(input_number):
    input_number = check_input_validity(input_number)
    number_into_primes(input_number)


run(numberInput)






